function enviar() {
    // Guardamos en cada variable lo valores que ha escrito el usuario
    let direccion = document.querySelector('#direccion').value;
    let poblacion = document.querySelector('#poblacion').value;
    let provincia = document.querySelector('#provincia').value;

    // Mensaje de confirmacion de que se han introducido los datos
    document.querySelector('.datos').innerHTML = "Los datos se han introducido correctamente " + '<i class="bi bi-check-circle-fill"></i>';
    // Modificamos el HTML con los datos introducidos
    document.querySelector('.resultado').innerHTML = "Dirección: " + direccion + "<br>Población: " + poblacion + "<br>Provincia: " + provincia;
}